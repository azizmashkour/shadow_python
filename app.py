
# importing Flask, jsonify, request and PyMongo libraries
from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
from os import environ

app = Flask(__name__)

# Begin Data base configs
# Data base config in prod environment
# app.config['MONGO_DBNAME'] = environ.get('MONGODB_DB')
# app.config['MONGO_URI'] = environ.get('MONGODB_URI')

# Data base config in dev environment
app.config['MONGO_DBNAME'] = 'restdb'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/restdb'
mongo = PyMongo(app)

# End Data base configs

# Route configurations
@app.route("/")
def hello():
    return "this is the home page"

# Begin of user routes: add, get one and all users
#### TODO: Edit user
#### TODO: Delete user

@app.route('/user', methods=['POST'])
def add_user():
  user = mongo.db.users
  first_name = request.json['first_name']
  last_name = request.json['last_name']
  user_id = user.insert({'first_name': first_name, 'last_name': last_name})
  new_user = user.find_one({'_id': user_id })
  output = {'first_name' : new_user['first_name'], 'last_name' : new_user['last_name']}
  return jsonify({'result' : output})

@app.route('/user/<name>', methods=['GET'])
def get_one_user(name):
  user = mongo.db.users
  s = user.find_one({'first_name' : name})
  if s:
    output = {'first_name' : s['first_name'], 'last_name' : s['last_name']}
  else:
    output = "No user found with such name"
  return jsonify({'result' : output})

@app.route('/users', methods=['GET'])
def get_all_users():
  user = mongo.db.users
  output = []
  for s in user.find():
    output.append({'first_name' : s['first_name'], 'last_name' : s['last_name']})
  return jsonify({'result' : output})

# End of user routes: add, get one or all users

# Begin of user routes: add, get one or all users
#### TODO: Edit notaire
#### TODO: Delete notaire

@app.route('/notaire', methods=['POST'])
def add_notaire():
  notaire = mongo.db.notaires
  first_name = request.json['first_name']
  last_name = request.json['last_name']
  available_time = request.json['available_time']
  password = request.json['password']
  notaire_id = notaire.insert({'first_name': first_name, 'last_name': last_name, 'password': password, 'available_time': available_time})
  new_notaire = notaire.find_one({'_id': notaire_id })
  output = {'first_name' : new_notaire['first_name'], 'last_name' : new_notaire['last_name'],
            'password': new_notaire['password'], 'available_time': new_notaire['available_time']}
  return jsonify({'result' : output})

@app.route('/notaire/<name>', methods=['GET'])
def get_one_notaire(name):
  notaire = mongo.db.notaires
  s = notaire.find_one({'first_name' : name})
  if s:
    output = {'first_name' : s['first_name'], 'last_name' : s['last_name'], 'available_time': s['available_time']}
  else:
    output = "No notaire found with such name"
  return jsonify({'result' : output})

@app.route('/notaires', methods=['GET'])
def get_all_notaires():
  notaire = mongo.db.notaires
  output = []
  for s in notaire.find():
    output.append({'first_name' : s['first_name'], 'last_name' : s['last_name']})
  return jsonify({'result' : output})

# End of notaire routes: add, get one or all users

# Begin of rendez-vous(rdv) routes: add, get one or all rendez-vous(rdv)
#### TODO: Edit rendez-vous
#### TODO: Delete rendez-vous

@app.route('/rdv', methods=['POST'])
def add_rdv():
  rdv = mongo.db.rdvs
  title = request.json['title']
  notaire_id = request.json['notaire_id']
  user_id = request.json['user_id']
  rdv_time = request.json['rdv_time']
  rdv_id = rdv.insert({'title': title, 'notaire_id': notaire_id, 'user_id': user_id, 'rdv_time': rdv_time})
  new_rdv = rdv.find_one({'_id': rdv_id })
  output = {'title' : new_rdv['title'], 'notaire_id' : new_rdv['notaire_id'],
            'user_id': new_rdv['user_id'], 'rdv_time': new_rdv['rdv_time']}
  return jsonify({'result' : output})

@app.route('/rdv/<title>', methods=['GET'])
def get_one_rdv(title):
  rdv = mongo.db.rdvs
  s = rdv.find_one({'title' : title})
  if s:
    output = {'title' : s['title'], 'notaire_id' : s['notaire_id'], 'user_id': s['user_id'], 'rdv_time': s['rdv_time']}
  else:
    output = "No rdv found with such title"
  return jsonify({'result' : output})

@app.route('/rdvs', methods=['GET'])
def get_all_rdv():
  rdv = mongo.db.rdvs
  output = []
  for s in rdv.find():
    output.append({'title' : s['title'], 'notaire_id' : s['notaire_id'], 'user_id': s['user_id'], 'rdv_time': s['rdv_time']})
  return jsonify({'result' : output})

# End of rendez-vous(rdv) routes: add, get one or all rendez-vous(rdv)

# Begin of admin user routes: add, get one and all admin users
#### TODO: Edit admin user
#### TODO: Delete admin user

@app.route('/admin', methods=['POST'])
def add_admin():
  admin = mongo.db.admins
  username = request.json['username']
  password = request.json['password']
  admin_id = admin.insert({'username': username, 'password': password})
  new_admin = admin.find_one({'_id': admin_id })
  output = {'username' : new_user['username'], 'password' : new_user['password']}
  return jsonify({'result' : output})

@app.route('/admin/<username>', methods=['GET'])
def get_one_admin(username):
  admin = mongo.db.admins
  s = admin.find_one({'username' : username})
  if s:
    output = {'username' : s['username']}
  else:
    output = "No admin found with such username"
  return jsonify({'result' : output})

@app.route('/admins', methods=['GET'])
def get_all_admins():
  admin = mongo.db.admins
  output = []
  for s in admin.find():
    output.append({'username' : s['username']})
  return jsonify({'result' : output})
# End of admin routes: add, get one or all admin users

if __name__ == "__main__":
    app.run(debug=True)
